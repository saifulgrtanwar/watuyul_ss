<html>

<head>

    <?php if ($link->pixel_id && $link->pixel_event) { ?>

    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '<?php echo $link->pixel_id; ?>');

        <?php if ($link->pixel_event == 'Purchase' && $link->pixel_event_data) { ?>
            fbq('track', '<?php echo $link->pixel_event; ?>', JSON.parse('<?php echo $link->pixel_event_data; ?>'));
        <?php } else { ?>
            fbq('track', '<?php echo $link->pixel_event; ?>');
        <?php } ?>
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=<?php echo $link->pixel_id; ?>&ev=<?php echo $link->pixel_event; ?>&noscript=1"
        /></noscript>

    <?php } ?>
    <?php if ($link->gtm_id) { ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo $link->gtm_id; ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MRFXMGK"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php } ?>

    <script>

        window.addEventListener('load', (event) => {
            setTimeout(() => {
                window.location = '<?php echo $url; ?>'
            }, 1000 * <?php echo $link->loading; ?>)
        })

    </script>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <style>

        body {
            background: linear-gradient(175deg, rgba(76,72,131,1) 0%, rgba(135,68,156,1) 33%, rgba(74,173,193,1) 100%);
            font-family: Monserrat, 'sans-serif';
            padding: 0;
            margin: 0;
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        h2 {
            color: white;
            margin: 0;
        }

        h3 {
            color: white;
            margin: 0;
            font-size: 15px;
            font-weight: 400;
        }

    </style>

</head>

<body>

<h2>Sedang Memuat Link</h2>
<h3>Anda akan segera diarahkan ke halaman chat...</h3>

</body>

</html>
