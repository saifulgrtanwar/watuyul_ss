<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::get('links/{slug}', 'LinkController@view');

Route::middleware('auth:api')->group(function() {
    Route::prefix('user')->group(function () {
        Route::get('/account_type', 'UserController@getAccountType');
        Route::get('/{id}', 'UserController@getById');
        Route::get('', 'UserController@index');
        Route::post('', 'UserController@create');
        Route::put('/{id}', 'UserController@update');
        Route::delete('/many', 'UserController@deleteMany');
        Route::delete('/{id}', 'UserController@delete');
    });

    Route::prefix('account_type')->group(function () {
        Route::get('/{id}', 'AccountTypeController@getById');
        Route::get('', 'AccountTypeController@index');
        Route::post('', 'AccountTypeController@create');
        Route::put('/{id}', 'AccountTypeController@update');
        Route::delete('/many', 'AccountTypeController@deleteMany');
        Route::delete('/{id}', 'AccountTypeController@delete');
    });

    Route::prefix('client')->group(function () {
        Route::get('/{id}', 'ClientController@getById');
        Route::put('/{id}', 'ClientController@update');
        Route::delete('/many', 'ClientController@deleteMany');
        Route::delete('/{id}', 'ClientController@delete');
        Route::get('', 'ClientController@index');
        Route::post('/use/{id}', 'ClientController@use');
        Route::post('/release/{id}', 'ClientController@release');
        Route::post('', 'ClientController@create');
    });

    Route::prefix('contact_set')->group(function () {
        Route::get('/{id}', 'ContactSetController@getById');
        Route::put('/{id}', 'ContactSetController@update');
        Route::delete('/many', 'ContactSetController@deleteMany');
        Route::delete('/{id}', 'ContactSetController@delete');
        Route::get('', 'ContactSetController@index');
        Route::post('', 'ContactSetController@create');
    });

    Route::prefix('contact')->group(function () {
        Route::get('/by_set/{id}', 'ContactController@getBySet');
        Route::get('/{id}', 'ContactController@getById');
        Route::get('', 'ContactController@index');
        Route::put('/{id}', 'ContactController@update');
        Route::post('/many', 'ContactController@createMany');
        Route::post('', 'ContactController@create');
        Route::delete('/many', 'ContactController@deleteMany');
        Route::delete('/{id}', 'ContactController@delete');
    });

    Route::prefix('broadcast')->group(function () {
        Route::get('/{id}', 'BroadcastController@getById');
        Route::get('', 'BroadcastController@index');
        Route::put('/{id}', 'BroadcastController@update');
        Route::post('', 'BroadcastController@create');
        Route::delete('/many', 'BroadcastController@deleteMany');
        Route::delete('/{id}', 'BroadcastController@delete');
    });

    Route::prefix('operator')->group(function () {
        Route::get('/{id}', 'OperatorController@getById');
        Route::put('/{id}', 'OperatorController@update');
        Route::delete('/many', 'OperatorController@deleteMany');
        Route::delete('/{id}', 'OperatorController@delete');
        Route::get('', 'OperatorController@index');
        Route::post('', 'OperatorController@create');
    });

    Route::prefix('link')->group(function () {
        Route::get('/{id}', 'LinkController@getById');
        Route::put('/{id}', 'LinkController@update');
        Route::delete('/many', 'LinkController@deleteMany');
        Route::delete('/{id}', 'LinkController@delete');
        Route::get('', 'LinkController@index');
        Route::post('', 'LinkController@create');
    });

    Route::prefix('message')->group(function () {
        Route::get('/by_client/{id}', 'MessageController@getByClient');
        Route::get('/by_broadcast/{id}', 'MessageController@getByBroadcast');
        Route::get('/dequeue/{id}', 'MessageController@dequeue');
        Route::get('/{id}', 'MessageController@getById');
        Route::get('', 'MessageController@index');
        Route::put('/{id}', 'MessageController@update');
        Route::delete('/many', 'MessageController@deleteMany');
        Route::delete('/{id}', 'MessageController@delete');
    });

    Route::prefix('domain')->group(function () {
        Route::get('/{id}', 'DomainController@getById');
        Route::put('/{id}', 'DomainController@update');
        Route::delete('/many', 'DomainController@deleteMany');
        Route::delete('/{id}', 'DomainController@delete');
        Route::get('', 'DomainController@index');
        Route::post('/use/{id}', 'DomainController@use');
        Route::post('/release/{id}', 'DomainController@release');
        Route::post('', 'DomainController@create');
    });
});

