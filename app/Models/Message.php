<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "message";
    public $timestamps = true;

    protected $with = ['client'];
    protected $fillable = [
        'user_id', 'client_id', 'broadcast_id', 'contact_id', 'name', 'number', 'message', 'send_time', 'status', 'sent_at',
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function getMessageAttribute()
    {
        return json_decode($this->attributes['message'], true);
    }
}
