<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Broadcast extends Model
{
    protected $table = "broadcast";
    public $timestamps = true;

    protected $fillable = [
        'name', 'user_id', 'client_id', 'contact_set_ids',
        'contents', 'send_option', 'send_time', 'status',
        'max_message_per_request', 'max_message_per_minute'
    ];

    protected $with = ['client'];
    protected $appends = [
        'contact_set', 'total_contacts',
        'total_messages', 'pending_messages', 'sent_messages', 'not_sent_messages'
    ];
    protected $hidden = ['contents', 'contact_set_ids'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function getContactSetAttribute()
    {
        return ContactSet::query()->whereIn('id', json_decode($this->contact_set_ids))->get();
    }

    public function getTotalContactsAttribute()
    {
        $contactSetIds = implode(',', json_decode($this->contact_set_ids));
        return DB::select(
            DB::raw(
                "SELECT COUNT(*) as count FROM contact WHERE contact_set_id IN ($contactSetIds)"
            )
        )[0]->count;
    }

    public function getTotalMessagesAttribute()
    {
        return Message::query()->where('broadcast_id', $this->id)->count();
    }

    public function getPendingMessagesAttribute()
    {
        return Message::query()
            ->where('broadcast_id', $this->id)
            ->where('status', '=', 'Pending')
            ->count();
    }

    public function getSentMessagesAttribute()
    {
        return Message::query()
            ->where('broadcast_id', $this->id)
            ->where('status', '=', 'Sent')
            ->count();
    }

    public function getNotSentMessagesAttribute()
    {
        return Message::query()
            ->where('broadcast_id', $this->id)
            ->where('status', '=', 'Not Sent')
            ->count();
    }
}
