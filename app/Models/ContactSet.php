<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ContactSet extends Model
{
    protected $table = "contact_set";
    public $timestamps = true;

    protected $fillable = ['user_id', 'name'];
    protected $appends = ['contact_count'];

    public function getContactCountAttribute()
    {
        return Contact::query()->where('contact_set_id', $this->id)->count();
    }
}
