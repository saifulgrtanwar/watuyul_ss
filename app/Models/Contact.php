<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "contact";
    public $timestamps = true;

    protected $fillable = ['contact_set_id', 'user_id', 'name', 'number'];
}
