<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = "link";
    public $timestamps = true;

    protected $with = ['domain'];
    protected $fillable = [
        'user_id', 'title', 'slug', 'operators', 'chat_content', 'status', 'clicks',
        'pixel_id', 'pixel_event', 'pixel_event_data', 'gtm_id', 'loading', 'domain_id'
    ];

    public function getOperatorsAttribute()
    {
        $ops = json_decode($this->attributes['operators'], true);
        $fOps = [];

        foreach ($ops as $op)
        {
            $fOps[] = [
                'id' => $op['id'],
                'name' => Operator::query()->find($op['id'])->name,
                'priority' => $op['priority']
            ];
        }

        return $fOps;
    }

    public function domain()
    {
        return $this->belongsTo('App\Models\Domain', 'domain_id');
    }
}
