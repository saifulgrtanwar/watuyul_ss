<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $table = "operator";
    public $timestamps = true;

    protected $fillable = ['user_id', 'name', 'number', 'clicks'];
    protected $appends = ['links'];

    public function getLinksAttribute()
    {
        $links = Link::query()->where('user_id', $this->user_id)->get();
        $total = 0;

        foreach ($links as $link)
        {
            foreach ($link->operators as $op)
            {
                if ($op['id'] == $this->id)
                {
                    $total++;
                    break;
                }
            }
        }

        return $total;
    }
}
