<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = "domain";
    public $timestamps = false;

    protected $fillable = ['name'];
}
