<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    protected $table = "account_type";
    public $timestamps = false;

    protected $fillable = [
        'type',
        'max_client',
        'max_contact_set',
        'max_contact',
        'max_broadcast',
        'max_message',
        'max_operator',
        'max_link',
    ];
}
