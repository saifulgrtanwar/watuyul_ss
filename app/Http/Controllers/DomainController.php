<?php

namespace App\Http\Controllers;

use App\Models\Domain;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    public function index()
    {
        return $this->doSuccess(Domain::all());
    }

    public function getById($id)
    {
        $domain = Domain::query()->find($id);

        if (!$domain)
            return $this->doError("Domain not found");

        return $this->doSuccess($domain);
    }

    public function create(Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['name']);

        $domain = Domain::query()->create([
            'name' => $request->name
        ]);

        return $this->doSuccess($domain);
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['name']);

        $domain = Domain::query()->find($id);

        if (!$domain)
            return $this->doError("Domain not found");

        $domain->update([
            'name' => $request->name
        ]);

        return $this->doSuccess($domain);
    }

    public function delete($id)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $domain = Domain::query()->find($id);

        if (!$domain)
            return $this->doError("Domain not found");

        $rows = $domain->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['ids']);

        $domains = Domain::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($domains as $domain)
        {
            if ($domain->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }
}
