<?php

namespace App\Http\Controllers;

use App\Models\Broadcast;
use App\Models\Contact;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class BroadcastController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(
            Broadcast::query()
                ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->get()
        );
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;
        $broadcast = Broadcast::query()->find($id)->makeVisible('contents');

        if (!$broadcast)
            return $this->doError("Broadcast not found");
        else if ($broadcast->user_id != $user_id)
            return $this->doError("Broadcast is not valid");

        $broadcast->contents = json_decode($broadcast->contents, true);

        return $this->doSuccess($broadcast);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name', 'client_id', 'contact_set_ids', 'contents', 'send_option', 'status',
            'max_message_per_request', 'max_message_per_minute',
        ]);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_broadcast;

        if ($maxData >= 0 && Broadcast::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData broadcasts");
        }

        $broadcast = Broadcast::query()->create([
            'user_id' => $user_id,
            'name' => $request->name,
            'client_id' => $request->client_id,
            'contact_set_ids' => json_encode($request->contact_set_ids),
            'contents' => $this->formatContents($request->contents),
            'send_option' => $request->send_option,
            'send_time' => $request->send_option === 'custom' ? $request->send_time : null,
            'status' => $request->status,
            'max_message_per_request' => $request->max_message_per_request,
            'max_message_per_minute' => $request->max_message_per_minute,
        ]);

        if ($request->status === 'Published')
            $this->broadcastMessage($broadcast, false);

        return $this->doSuccess($broadcast);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name', 'contents', 'send_option', 'status',
            'max_message_per_request', 'max_message_per_minute',
        ]);

        $user_id = auth()->user()->id;
        $broadcast = Broadcast::query()->find($id);

        if (!$broadcast)
            return $this->doError("Broadcast not found");
        else if ($broadcast->user_id != $user_id)
            return $this->doError("Broadcast is not valid");

        $update = $broadcast->status === 'Published';

        $broadcast->update([
            'name' => $request->name,
            'contents' => $this->formatContents($request->contents),
            'send_option' => $request->send_option,
            'send_time' => $request->send_option === 'custom' ? $request->send_time : null,
            'status' => $request->status,
            'max_message_per_request' => $request->max_message_per_request,
            'max_message_per_minute' => $request->max_message_per_minute,
        ]);

        if ($request->status === 'Published')
            $this->broadcastMessage($broadcast, $update);

        return $this->doSuccess($broadcast);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $broadcast = Broadcast::query()->find($id);

        if (!$broadcast)
            return $this->doError("Broadcast not found");
        else if ($broadcast->user_id != $user_id)
            return $this->doError("Broadcast is not valid");

        $rows = $broadcast->delete();

        Message::query()->where('broadcast_id', $broadcast->id)->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $broadcasts = Broadcast::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($broadcasts as $broadcast)
        {
            if ($broadcast->user_id != $user_id)
                continue;

            if ($broadcast->delete()) $deleted++;

            Message::query()->where('broadcast_id', $broadcast->id)->delete();
        }

        return $this->doSuccess("$deleted rows deleted");
    }

    function formatContents($contents)
    {
        $formatted = [];

        foreach ($contents as $content)
        {
            $c = [ 'text' => $content['text'] ];

            if (isset($content['image']))
            {
                if (strlen($content['image']) >= 5 && substr($content['image'], 0, 5) === 'data:')
                    $c['image'] = $this->saveBase64File($content['image']);
                else
                    $c['image'] = $content['image'];
            }

            if (isset($content['video']))
            {
                if (strlen($content['video']) >= 5 && substr($content['video'], 0, 5) === 'data:')
                    $c['video'] = $this->saveBase64File($content['video']);
                else
                    $c['video'] = $content['video'];
            }

            $formatted[] = $c;
        }

        return json_encode($formatted);
    }

    function saveBase64File($data)
    {
        /*
         * data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH
         */

        $datas = explode(',', $data);
        $base64 = $datas[1];
        $extension = explode(';', explode('/', $datas[0])[1])[0];
        $fileName = Str::random(20) . '.' . $extension;

        $content = base64_decode($base64);
        $destPath = public_path() . '/uploads/' . $fileName;
        file_put_contents($destPath, $content);

        return '/uploads/' . $fileName;
    }

    function broadcastMessage($broadcast, $update)
    {
        $contactSets = $broadcast->contact_set;
        $msgs = json_decode($broadcast->contents, true);
        $msgCount = 0;

        if (!$update)
        {
            foreach ($contactSets as $contactSet)
            {
                $contacts = Contact::query()->where('contact_set_id', $contactSet->id)->get();

                foreach ($contacts as $contact)
                {
                    if (Message::query()->where('number', $contact->number)->where('broadcast_id', $broadcast->id)->first())
                        continue;

                    $msg = $msgs[$msgCount % count($msgs)];
                    $msg["text"] = str_replace('[name]', $contact->name, $msg['text']);

                    Message::query()->create([
                        'user_id' => $broadcast->user_id,
                        'broadcast_id' => $broadcast->id,
                        'client_id' => $broadcast->client_id,
                        'contact_id' => $contact->id,
                        'name' => $contact->name,
                        'number' => $contact->number,
                        'message' => json_encode($msg),
                        'send_time' => $broadcast->send_option === 'immediate' ? Carbon::now() : $broadcast->send_time
                    ]);

                    $msgCount++;
                }
            }
        }
        else
        {
            $messages = Message::query()
                ->where('broadcast_id', $broadcast->id)
                ->where('status', 'Pending')
                ->get();

            foreach ($messages as $message)
            {
                $contact = Contact::query()->find($message->contact_id);

                $msg = $msgs[$msgCount % count($msgs)];

                if ($contact)
                    $msg["text"] = str_replace('[name]', $contact->name, $msg['text']);

                $message->update([
                    'message' => json_encode($msg),
                    'send_time' => $broadcast->send_option === 'immediate' ? Carbon::now() : $broadcast->send_time
                ]);

                $msgCount++;
            }
        }

        return $msgCount;
    }
}
