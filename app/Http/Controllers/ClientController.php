<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(Client::query()->where('user_id', $user_id)->get());
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;

        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        return $this->doSuccess($client);
    }

    public function create(Request $request)
    {
        $this->validate($request, ['name']);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_client;

        if ($maxData >= 0 && Client::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData clients");
        }

        $client = Client::query()->create([
            'user_id' => $user_id,
            'name' => $request->name
        ]);

        return $this->doSuccess($client);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name']);

        $user_id = auth()->user()->id;
        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        $client->update([
            'name' => $request->name
        ]);

        return $this->doSuccess($client);
    }

    public function use($id)
    {
    	$user_id = auth()->user()->id;
        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");
        else if ($client->status == 'Used')
        	return $this->doError("Client already in use");

        $client->update([
            'status' => 'Used'
        ]);

        return $this->doSuccess($client);
    }

    public function release($id)
    {
    	$user_id = auth()->user()->id;
        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        $client->update([
            'status' => 'Released'
        ]);

        return $this->doSuccess($client);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        $rows = $client->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $clients = Client::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($clients as $client)
        {
            if ($client->user_id != $user_id)
                continue;

            if ($client->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }
}
