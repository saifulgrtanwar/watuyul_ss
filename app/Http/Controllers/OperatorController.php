<?php

namespace App\Http\Controllers;

use App\Models\Operator;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(Operator::query()->where('user_id', $user_id)->get());
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;

        $operator = Operator::query()->find($id);

        if (!$operator)
            return $this->doError("Operator not found");
        else if ($operator->user_id != $user_id)
            return $this->doError("Operator is not valid");

        return $this->doSuccess($operator);
    }

    public function create(Request $request)
    {
        $this->validate($request, ['name', 'number']);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_operator;

        if ($maxData >= 0 && Operator::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData operators");
        }

        $operator = Operator::query()->create([
            'user_id' => $user_id,
            'name' => $request->name,
            'number' => $this->formatNumber($request->number),
        ]);

        return $this->doSuccess($operator);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name', 'number']);

        $user_id = auth()->user()->id;
        $operator = Operator::query()->find($id);

        if (!$operator)
            return $this->doError("Operator not found");
        else if ($operator->user_id != $user_id)
            return $this->doError("Operator is not valid");

        $operator->update([
            'name' => $request->name,
            'number' => $this->formatNumber($request->number),
        ]);

        return $this->doSuccess($operator);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $operator = Operator::query()->find($id);

        if (!$operator)
            return $this->doError("Operator not found");
        else if ($operator->user_id != $user_id)
            return $this->doError("Operator is not valid");

        $rows = $operator->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $operators = Operator::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($operators as $operator)
        {
            if ($operator->user_id != $user_id)
                continue;

            if ($operator->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }

    function formatNumber($number)
    {
        if (substr($number, 0, 1) === '0') {
            $n = "62" . substr($number, 1);
        }
        else if (substr($number, 0, 1) === '+') {
            $n = substr($number, 1);
        }
        else {
            $n = $number;
        }

        $n = str_replace(" ", "", $n);
        $n = str_replace("-", "", $n);
        $n = str_replace('"', "", $n);

        return $n;
    }
}
