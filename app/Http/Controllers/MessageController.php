<?php

namespace App\Http\Controllers;

use App\Models\Broadcast;
use App\Models\Client;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MessageController extends Controller
{
    const MESSAGE_PER_REQUEST = 3;

    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(
            Message::query()
                ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->get()
        );
    }

    public function getByClient($id)
    {
        $user_id = auth()->user()->id;
        $client = Client::query()->find($id);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        $messages = Message::query()->where('client_id', $id)->where('user_id', $user_id)->get();

        return $this->doSuccess($messages);
    }

    public function getByBroadcast($id)
    {
        $user_id = auth()->user()->id;
        $broadcast = Broadcast::query()->find($id);

        if (!$broadcast)
            return $this->doError("Broadcast not found");
        else if ($broadcast->user_id != $user_id)
            return $this->doError("Broadcast is not valid");

        $messages = Message::query()->where('broadcast_id', $id)->get();

        return $this->doSuccess($messages);
    }

    public function dequeue($clientId)
    {
        $user_id = auth()->user()->id;
        $client = Client::query()->find($clientId);

        if (!$client)
            return $this->doError("Client not found");
        else if ($client->user_id != $user_id)
            return $this->doError("Client is not valid");

        $client->update([
            'last_request' => Carbon::now()
        ]);

        $messages = Message::query()
            ->where('client_id', $clientId)
            ->where('user_id', $user_id)
            ->where('send_time', '<=', Carbon::now())
            ->where('status', 'Pending')
            ->orderBy('id', 'asc')
            ->limit(5)
            ->get();

        $limitedMessages = [];

        if (count($messages))
        {
            $broadcast = Broadcast::query()->where('id', $messages[0]->broadcast_id)->first();

            if ($broadcast)
            {
                $lastMinuteMessageCount = Message::query()
                    ->where('client_id', $clientId)
                    ->where('user_id', $user_id)
                    ->where('sent_at', '>=', Carbon::now()->subMinutes(1))
                    ->where('status', '!=', 'Pending')
                    ->count();

                $maxAllowedMessage = $broadcast->max_message_per_minute - $lastMinuteMessageCount;

                if ($maxAllowedMessage < $broadcast->max_message_per_request)
                    $maxMessage = $maxAllowedMessage;
                else
                    $maxMessage = $broadcast->max_message_per_request;

                if ($maxMessage > 0)
                {
                    if (count($messages) > $maxMessage)
                    {
                        for ($a = 0; $a < $maxMessage; $a++)
                        {
                            $limitedMessages[] = $messages[$a];
                        }
                    }
                    else
                    {
                        $limitedMessages = $messages;
                    }
                }
            }
            else
            {
                $limitedMessages = $messages;
            }
        }

        return $this->doSuccess($limitedMessages);
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;
        $message = Message::query()->find($id);

        if (!$message)
            return $this->doError("Message not found");
        else if ($message->user_id != $user_id)
            return $this->doError("Message is not valid");

        return $this->doSuccess($message);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'status'
        ]);

        $user_id = auth()->user()->id;
        $message = Message::query()->find($id);

        if (!$message)
            return $this->doError("Message not found");
        else if ($message->user_id != $user_id)
            return $this->doError("Message is not valid");

        $message->update([
            'status' => $request->status,
            'sent_at' => Carbon::now(),
        ]);

        return $this->doSuccess($message);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $message = Message::query()->find($id);

        if (!$message)
            return $this->doError("Message not found");
        else if ($message->user_id != $user_id)
            return $this->doError("Message is not valid");

        $rows = $message->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $messages = Message::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($messages as $message)
        {
            if ($message->user_id != $user_id)
                continue;

            if ($message->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }
}
