<?php


namespace App\Http\Controllers;


use App\Models\Client;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function login() {
        $credentials = request(['email', 'password']);
        $token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addCenturies(7)->timestamp]);

        if (!$token) {
            return $this->doError("Email or password is incorrect");
        }
        else
            return $this->doSuccess([
                'user' => Auth::user(),
                'access_token' => $token,
                'token_type' => 'bearer',
            ]);
    }

    public function index() {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        return $this->doSuccess(User::all());
    }

    public function getById($id) {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $user = User::query()->find($id);

        if (!$user)
            return $this->doError("User not found");

        return $this->doSuccess($user);
    }

    public function getAccountType() {
        return $this->doSuccess(auth()->user()->account_type);
    }

    public function create(Request $request) {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['name', 'email', 'password', 'account_type_id']);

        if (User::query()->where('email', $request->email)->first())
            $this->doError("Email already registered");

        $user = User::query()->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'account_type_id' => $request->account_type_id,
        ]);

        return $this->doSuccess(User::query()->find($user->id));
    }

    public function update($id, Request $request) {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        if ($request->email && User::query()->where('email', $request->email)->where('id', '!=', $id)->first())
            return $this->doError("Email already registered");

        $user = User::query()->find($id);

        if (!$user)
            return $this->doError("User not found");

        $data = [];

        if ($request->name) $data['name'] = $request->name;
        if ($request->email) $data['email'] = $request->email;
        if ($request->account_type_id) $data['account_type_id'] = $request->account_type_id;
        if ($request->password) $data['password'] = Hash::make($request->password);

        $user->update($data);

        return $this->doSuccess($user);
    }

    public function delete($id) {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $user = User::query()->find($id);

        if (!$user)
            return $this->doError("User not found");

        $rows = $user->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request) {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['ids']);

        $users = User::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($users as $user)
        {
            if ($user->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
//            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
