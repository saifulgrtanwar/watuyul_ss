<?php

namespace App\Http\Controllers;

use App\Models\AccountType;
use Illuminate\Http\Request;

class AccountTypeController extends Controller
{
    public function index()
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        return $this->doSuccess(AccountType::all());
    }

    public function getById($id)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $account_type = AccountType::query()->find($id);

        if (!$account_type)
            return $this->doError("AccountType not found");

        return $this->doSuccess($account_type);
    }

    public function create(Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, [
            'type', 'max_client', 'max_contact_set', 'max_contact', 'max_broadcast',
            'max_message', 'max_operator', 'max_link'
        ]);

        $account_type = AccountType::query()->create([
            'type' => $request->type,
            'max_client' => $request->max_client,
            'max_contact_set' => $request->max_contact_set,
            'max_contact' => $request->max_contact,
            'max_broadcast' => $request->max_broadcast,
            'max_message' => $request->max_message,
            'max_operator' => $request->max_operator,
            'max_link' => $request->max_link,
        ]);

        return $this->doSuccess($account_type);
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, [
            'type', 'max_client', 'max_contact_set', 'max_contact', 'max_broadcast',
            'max_message', 'max_operator', 'max_link'
        ]);

        $account_type = AccountType::query()->find($id);

        if (!$account_type)
            return $this->doError("AccountType not found");

        $account_type->update([
            'type' => $request->type,
            'max_client' => $request->max_client,
            'max_contact_set' => $request->max_contact_set,
            'max_contact' => $request->max_contact,
            'max_broadcast' => $request->max_broadcast,
            'max_message' => $request->max_message,
            'max_operator' => $request->max_operator,
            'max_link' => $request->max_link,
        ]);

        return $this->doSuccess($account_type);
    }

    public function delete($id)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $account_type = AccountType::query()->find($id);

        if (!$account_type)
            return $this->doError("AccountType not found");

        $rows = $account_type->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        if (auth()->user()->account_type_id != '1')
            return $this->doError("You don't have access to this API");

        $this->validate($request, ['ids']);

        $account_types = AccountType::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($account_types as $account_type)
        {
            if ($account_type->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }
}
