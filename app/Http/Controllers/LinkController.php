<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LinkController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(Link::query()->where('user_id', $user_id)->get());
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;

        $link = Link::query()->find($id);

        if (!$link)
            return $this->doError("Link not found");
        else if ($link->user_id != $user_id)
            return $this->doError("Link is not valid");

        return $this->doSuccess($link);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title', 'slug', 'operators', 'chat_content', 'status', 'loading', 'domain_id'
        ]);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_link;

        if ($maxData >= 0 && Link::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData links");
        }

        if (Link::query()->where('slug', $request->slug)->first())
            return $this->doError("Slug already used");

        $link = Link::query()->create([
            'user_id' => $user_id,
            'title' => $request->title,
            'slug' => $request->slug,
            'operators' => json_encode($request->operators),
            'chat_content' => $request->chat_content,
            'status' => $request->status,
            'pixel_id' => $request->pixel_id ? $request->pixel_id : '',
            'pixel_event' => $request->pixel_event ? $request->pixel_event : '',
            'pixel_event_data' => $request->pixel_event_data ? $request->pixel_event_data : '',
            'gtm_id' => $request->gtm_id ? $request->gtm_id : '',
            'loading' => $request->loading,
            'domain_id' => $request->domain_id,
        ]);

        return $this->doSuccess(Link::query()->find($link->id));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title', 'slug', 'operators', 'chat_content', 'status', 'loading', 'domain_id'
        ]);

        if (Link::query()->where('slug', $request->slug)->where('id', '!=', $id)->first())
            return $this->doError("Slug already used");

        $user_id = auth()->user()->id;
        $link = Link::query()->find($id);

        if (!$link)
            return $this->doError("Link not found");
        else if ($link->user_id != $user_id)
            return $this->doError("Link is not valid");

        $link->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'operators' => json_encode($request->operators),
            'chat_content' => $request->chat_content,
            'status' => $request->status,
            'pixel_id' => $request->pixel_id ? $request->pixel_id : '',
            'pixel_event' => $request->pixel_event ? $request->pixel_event : '',
            'pixel_event_data' => $request->pixel_event_data ? $request->pixel_event_data : '',
            'gtm_id' => $request->gtm_id ? $request->gtm_id : '',
            'loading' => $request->loading,
            'domain_id' => $request->domain_id,
        ]);

        return $this->doSuccess($link);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $link = Link::query()->find($id);

        if (!$link)
            return $this->doError("Link not found");
        else if ($link->user_id != $user_id)
            return $this->doError("Link is not valid");

        $rows = $link->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $links = Link::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($links as $link)
        {
            if ($link->user_id != $user_id)
                continue;

            if ($link->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }

    public function view($slug)
    {
        $link = Link::query()->where('slug', $slug)->first();

        if ($link)
        {
            if ($link->status != 'Published')
            {
                echo "LINK TIDAK AKTIF";
                die();
            }

            $clicks = $link->clicks;
            $operators = $link->operators;

            $prioritySum = 0;
            $priorities = [];

            foreach ($operators as $op)
            {
                $prioritySum += $op['priority'];
                $priorities[] = $op['priority'];
            }

            $priorityRem = $clicks % $prioritySum;
            $opIndexes = [];

            while (true)
            {
                $isDone = true;

                for ($a = 0; $a < count($priorities); $a++)
                {
                    if ($priorities[$a] > 0)
                    {
                        $opIndexes[] = $a;
                        $priorities[$a]--;
                        $isDone = false;
                    }
                }

                if ($isDone) break;
            }

            $link->update([
                'clicks' => $link->clicks + 1
            ]);

            $opId = $operators[$opIndexes[$priorityRem]]['id'];

            $operator = Operator::query()->find($opId);

            if ($operator)
            {
                $operator->update([
                    'clicks' => $operator->clicks + 1
                ]);

                $phone = $operator->number;
                $text = $link->chat_content;

                $text = str_replace('[opname]', $operator->name, $text);
                $text = str_replace('[opnumber]', $operator->number, $text);
                $text = urlencode($text);

                return view('view_link', ['link' => $link, 'url' => "https://api.whatsapp.com/send?phone=$phone&text=$text"]);
            }
            else
                echo "ERROR";
        }
        else
        {
            echo "INVALID LINK";
        }
    }
}
