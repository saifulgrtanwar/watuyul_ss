<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\ContactSet;
use Illuminate\Http\Request;

class ContactSetController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(ContactSet::query()->where('user_id', $user_id)->get());
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;

        $contact_set = ContactSet::query()->find($id);

        if (!$contact_set)
            return $this->doError("ContactSet not found");
        else if ($contact_set->user_id != $user_id)
            return $this->doError("ContactSet is not valid");

        return $this->doSuccess($contact_set);
    }

    public function create(Request $request)
    {
        $this->validate($request, ['name']);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_contact_set;

        if ($maxData >= 0 && ContactSet::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData contact sets");
        }

        $contact_set = ContactSet::query()->create([
            'user_id' => $user_id,
            'name' => $request->name
        ]);

        return $this->doSuccess($contact_set);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name']);

        $user_id = auth()->user()->id;
        $contact_set = ContactSet::query()->find($id);

        if (!$contact_set)
            return $this->doError("ContactSet not found");
        else if ($contact_set->user_id != $user_id)
            return $this->doError("ContactSet is not valid");

        $contact_set->update([
            'name' => $request->name
        ]);

        return $this->doSuccess($contact_set);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $contact_set = ContactSet::query()->find($id);

        if (!$contact_set)
            return $this->doError("ContactSet not found");
        else if ($contact_set->user_id != $user_id)
            return $this->doError("ContactSet is not valid");

        $rows = $contact_set->delete();

        Contact::query()->where('contact_set_id', $contact_set->id)->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $contact_sets = ContactSet::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($contact_sets as $contact_set)
        {
            if ($contact_set->user_id != $user_id)
                continue;

            if ($contact_set->delete()) $deleted++;

            Contact::query()->where('contact_set_id', $contact_set->id)->delete();
        }

        return $this->doSuccess("$deleted rows deleted");
    }
}
