<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\ContactSet;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;

        return $this->doSuccess(Contact::query()->where('user_id', $user_id)->get());
    }

    public function getById($id)
    {
        $user_id = auth()->user()->id;

        $contact = Contact::query()->find($id);

        if (!$contact)
            return $this->doError("Contact not found");
        else if ($contact->user_id != $user_id)
            return $this->doError("Contact is not valid");

        return $this->doSuccess($contact);
    }

    public function getBySet($id)
    {
        $user_id = auth()->user()->id;

        $contact_set = ContactSet::query()->find($id);

        if (!$contact_set)
            return $this->doError("Contact set not found");
        else if ($contact_set->user_id != $user_id)
            return $this->doError("Contact set is not valid");

        $contact = Contact::query()->where('contact_set_id', $id)->get();

        return $this->doSuccess([
            'total_contact' => Contact::query()->where('user_id', $user_id)->count(),
            'contact' => $contact,
        ]);
    }

    public function create(Request $request)
    {
        $this->validate($request, ['contact_set_id', 'name', 'number']);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_contact;

        if ($maxData >= 0 && Contact::query()->where('user_id', $user_id)->count() >= $maxData) {
            return $this->doError("You are only allowed to create $maxData contacts");
        }

        $contact = Contact::query()->create([
            'user_id' => $user_id,
            'contact_set_id' => $request->contact_set_id,
            'name' => $request->name,
            'number' => $this->formatNumber($request->number),
        ]);

        return $this->doSuccess($contact);
    }

    public function createMany(Request $request)
    {
        $this->validate($request, ['contact_set_id', 'items']);

        $user = auth()->user();
        $user_id = $user->id;
        $maxData = $user->account_type->max_contact;

        if ($maxData >= 0 && Contact::query()->where('user_id', $user_id)->count() + count($request->items) >= $maxData) {
            return $this->doError("You are only allowed to create $maxData contacts");
        }

        $items = [];

        foreach ($request->items as $i)
        {
            $items[] = [
                'user_id' => $user_id,
                'contact_set_id' => $request->contact_set_id,
                'name' => $i['name'],
                'number' => $this->formatNumber($i['number']),
            ];
        }

        $contact = Contact::query()->insert($items);

        return $this->doSuccess($contact);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name', 'number']);

        $user_id = auth()->user()->id;
        $contact = Contact::query()->find($id);

        if (!$contact)
            return $this->doError("Contact not found");
        else if ($contact->user_id != $user_id)
            return $this->doError("Contact is not valid");

        $contact->update([
            'name' => $request->name,
            'number' => $request->number,
        ]);

        return $this->doSuccess($contact);
    }

    public function delete($id)
    {
        $user_id = auth()->user()->id;
        $contact = Contact::query()->find($id);

        if (!$contact)
            return $this->doError("Contact not found");
        else if ($contact->user_id != $user_id)
            return $this->doError("Contact is not valid");

        $rows = $contact->delete();

        return $this->doSuccess($rows);
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, ['ids']);

        $user_id = auth()->user()->id;
        $contacts = Contact::query()->whereIn('id', $request->ids)->get();

        $deleted = 0;

        foreach ($contacts as $contact)
        {
            if ($contact->user_id != $user_id)
                continue;

            if ($contact->delete()) $deleted++;
        }

        return $this->doSuccess("$deleted rows deleted");
    }

    function formatNumber($number)
    {
        $n = str_replace(" ", "", $number);
        $n = str_replace("-", "", $n);
        $n = str_replace('"', "", $n);

        if (substr($n, 0, 1) === '0') {
            $n = "62" . substr($n, 1);
        }
        else if (substr($n, 0, 1) === '+') {
            $n = substr($n, 1);
        }
        else if (substr($n, 0, 2) !== '62') {
            $n = '62' . $n;
        }

        return $n;
    }
}
