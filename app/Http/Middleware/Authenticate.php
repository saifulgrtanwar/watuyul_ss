<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    public function handle($request, Closure $next, ...$guards)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$token || !$user) {
            return response()->json([
                'success' => false,
                'message' => "Unauthorized. Token expected",
                'data' => null
            ]);
        }

        return parent::handle($request, $next, ...$guards);
    }
}
